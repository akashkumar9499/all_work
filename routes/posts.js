const router = require('express').Router();
const verify = require('./verifyToken');
const employeeModel = require('../models/User2');
const Authors = require('../models/authors');
const books = require('../models/books');
//const pcRel = require('../model/relation');


const {registerValidation,loginValidation} = require('../validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});



router.post('/addemp',verify , upload.single('employeeImage'), async (req,res)=>{


    const user = new employeeModel({
        name: req.body.name,
        department:req.body.department,
        salary: req.body.salary,
        post: req.body.post,
        employeeImage: req.file.path 
    });
    try{
        const savedUser = await user.save();
        res.send({user: user._id });

    }catch(err){
        res.status(400).send(err);
    }




//     res.json({posts: {title:"My first post",description: "random data you shouldnt access"}
// });
});

router.get('/listofEmployees',verify,function(req,res,next){

    employeeModel.find({post:"employee"},function(err,response){
      if(err)
      res.send(err);
      else
      res.send({status: 200,resultfound:response.length, employee: response});
    });
      
  });

router.get('/listofAdmins',verify,function(req,res,next){

    employeeModel.find({post:"admin"},function(err,response){
      if(err)
      res.send(err);
      else
      res.send({status: 200,resultfound:response.length, admin: response});
    });
      
  });

router.put('/updateEmployeeImage',verify, upload.single('employeeImage'),function(req,res,next){
    const id=req.query.userId ;
    const image = req.file.path; 
    employeeModel.findByIdAndUpdate(id,{employeeImage: image}, function(err,response){
      if(err)
      res.send(err);
      else
      res.send({status: 200,  employee: response});
    });
      
  });

router.delete('/deleteEmp',verify,function(req,res,next){
    const id=req.query.userId ;
    employeeModel.findByIdAndDelete(id, function(err,response){
      if(err)
      res.send(err);
      else
      res.send({status: 200,  employee: response});
    });
      
  });

  router.post('/addempimage',verify, async (req,res,next)=>{


    const user = new employeeModel({
        name: req.body.name,
        department:req.body.department,
        salary: req.body.salary,
        post: req.body.post
    });
    try{
        const savedUser = await user.save();
        res.send({user: user._id });

    }catch(err){
        res.status(400).send(err);
    }




//     res.json({posts: {title:"My first post",description: "random data you shouldnt access"}
// });
});



router.post('/registerAuthor',async function(req, res) {

  //Adding new Author

  var authors = new Authors({
	  name: req.body.name,
	  age: req.body.age,
    booksid: req.body.booksid
    
  })
  //authors.Books.push(Books._id)
  
  authors.save(function(err){
	  if(err) return console.log(err.stack);
	  console.log("Author is added")
  });
  

  // const authors = new Authors(req.body);

  // try{
  //      const savedAuthor = await authors.save();
  //         res.send({authors: authors._id });
    
  //     }catch(err){
  //         res.status(400).send(err);
  //     }
  
});

//Register
 router.post('/registerBook', async(req, res)=> {

  const Books = new books(req.body);
  
  try{
      const savedBooks = await Books.save();
      res.send({books: Books});

  }catch(err){
      res.status(400).send(err);
  }

});

router.get('/findAuthor',async(req,res)=>{

  const data = Authors.find()
})

module.exports = router;