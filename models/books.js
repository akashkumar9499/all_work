const mongoose = require("mongoose");

const books = mongoose.Schema({

    name : String,
    page : Number
});

// export model 
module.exports = mongoose.model("books", books);

